import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import { TerzoCompComponent } from './terzo-comp/terzo-comp.component';
import { QuartoCompComponent } from './quarto-comp/quarto-comp.component';

@NgModule({
  declarations: [
    AppComponent,
    TerzoCompComponent,
    QuartoCompComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
